import java.util.Random;

/* Defines the simulation policy to use
 * A random simulator is used and it is associated
 * to a seeded random number generator.
 * A random number is generated, ifit is inferior to the 
 * children vector size it returns the corresponding child.
 * Otherwise, an other number is generated.
 */
public class ImgProcSimPolicy extends SimPolicy{

	public ImgProcOperationsBank operationsBank;
	static Random rndNumbers;
	static int randomVal;


	public ImgProcSimPolicy(){
		operationsBank = new ImgProcOperationsBank();
		ImgProcSimPolicy.rndNumbers = new Random();
		ImgProcSimPolicy.randomVal = 0;
	}

	public void setOperationsBank(ImgProcOperationsBank operationsBank2) {
		this.operationsBank = operationsBank2;
	}

	public void setseed(int seed){
		ImgProcSimPolicy.rndNumbers.setSeed(seed);
	}

	public Operation apply(Node node) {

		ImgProcNode newNode = new ImgProcNode();
		newNode = (ImgProcNode)node;

		if(newNode.hasChildren() == false){
			newNode.expend();
		}

		if(node.children.size()==1){
			ImgProcOperation p = new ImgProcOperation();
			p = (ImgProcOperation) newNode.children.get(0).op;
			return (Operation)p;
		}
		
		while(ImgProcSimPolicy.randomVal >= newNode.children.size()){
			ImgProcSimPolicy.randomVal = ImgProcSimPolicy.rndNumbers.nextInt(this.operationsBank.operations.size());
		}
		
		ImgProcOperation p = new ImgProcOperation();
		p = (ImgProcOperation) newNode.children.get(ImgProcSimPolicy.randomVal).op;

		ImgProcSimPolicy.randomVal = ImgProcSimPolicy.rndNumbers.nextInt(this.operationsBank.operations.size());
		
		return (Operation)p;
	}
}