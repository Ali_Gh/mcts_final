import java.io.File;
import java.util.LinkedList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class Test {
	public static void main (String[] arg){

		Reward reward = null;
		Node root = null;
		SimPolicy simPolicy = null;
		SelectPolicy selectPolicy = null;
		OperationsBank operationsBank = null;
		ImgProcSearchPolicyMcts mcts = new ImgProcSearchPolicyMcts();
		Path bestPath = new Path();
		LinkedList<Operation> bestBranch = new LinkedList<Operation>();


		// Load the OPENCV native library.
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		ImgProcReward imgProcReward = new ImgProcReward();
		ImgProcNode imgProcRoot = new ImgProcNode();
		ImgProcSimPolicy imgProcSimPolicy = new ImgProcSimPolicy();
		ImgProcOperationsBank imgProcOperationsBank = new ImgProcOperationsBank();
		ImgProcParameters param = new ImgProcParameters();
		ImgProcSelectPolicy imgProcSelectPolicy = new ImgProcSelectPolicy();

		/********** Set parameters **********/
		param.setParamFile(arg[0]);
		param.setDepth(param.getInt("depth", '='));
		param.setBudget(param.getInt("budget", '='));
		param.setNbRepetitions1(param.getInt("nbRepetition1", '='));
		param.setNbRepetitions2(param.getInt("nbRepetition2", '='));
		param.setHyperParam(param.getDouble("hyperParameter", '='));
		param.setSelectPolicy(param.getString("selectPolicy", '='));
		param.setSimPolicy(param.getString("simPolicy", '='));
		param.setOriginalAddress(param.getString("originalImgAddress", '='));
		param.setLabeledImgAddress(param.getString("labeledImgAddress", '='));
		param.setTestOriginalAddress(param.getString("testOriginalImgAddress", '='));
		param.setTestLabeledImgAddress(param.getString("testLabeledImgAddress", '='));
		param.setReportAdr(param.getString("reportAddress", '='));
		param.setBestPath(bestPath);
		param.setOs(param.getString("os", '='));
		param.setRam(param.getDouble("memory", '='));
		param.setProcessor(param.getString("processor", '='));
		param.setJavaVersion(param.getDouble("javaVersion", '='));
		param.setSeed(param.getInt("seed", '='));
		param.setMctsTemplate(param.getString("mctsTemplate", '='));
		param.setLearningImgSize(param.getInt("topX", '='), param.getInt("topY", '='), param.getInt("width", '='), param.getInt("height", '='));
		param.setCrop(param.getBoolean("crop", '='));
		param.setDownsample(param.getBoolean("downsample", '='));
		param.setDownsamplingRate(param.getDouble("downsamplingRate", '='));

		/*** Add filters features ***/
		
		param.addFilter(1, "Gabor", 28, 10, -1);
		param.addFilter(2, "Gabor", 28, 20, -1);
		param.addFilter(3, "Gabor", 28, 30, -1);
		param.addFilter(4, "Gabor", 28, 40, -1);
		param.addFilter(5, "Gabor", 28, 50, -1);
		param.addFilter(6, "Gabor", 28, 60, -1);
		param.addFilter(7, "Gabor", 28, 70, -1);
		param.addFilter(8, "Gabor", 28, 80, -1);
		param.addFilter(9, "Gabor", 28, 90, -1);
		param.addFilter(10, "Gabor", 28, 100, -1);

		param.addFilter(11, "Thresholding", -1, -1, -1);
		param.addFilter(12, "Thresholding", -1, -1, -1);
		param.addFilter(13, "Thresholding", -1, -1, -1);
		param.addFilter(14, "Thresholding", -1, -1, -1);
		param.addFilter(15, "Thresholding", -1, -1, -1);

		param.addFilter(16, "Gaussian", -1, 4, -1);
		param.addFilter(17, "Gaussian", -1, 6, -1);
		param.addFilter(18, "Gaussian", -1, 8, -1);
		param.addFilter(19, "Gaussian", -1, 16, -1);

		param.addFilter(20, "Opening", -1, -1, 1);
		param.addFilter(21, "Opening", -1, -1, 2);

		param.addFilter(22, "Closing", -1, -1, 1);
		param.addFilter(23, "Closing", -1, -1, 2);

		param.addFilter(24, "Stop", -1, -1, -1);

		/*** Operations bank building ***/
		
		for(int i = 1; i < 25; i++){

			ImgProcOperation op = new ImgProcOperation();
			op.num = i;

			if(i < 11){
				op.name = "Gabor";
			}else if(i < 16){
				op.name = "Thresholding";
			}else if(i < 20){
				op.name = "Gaussian";
			}else if(i < 22){
				op.name = "Opening";
			}else if(i < 24){
				op.name = "Closing";
			}else{
				op.name = "Stop";
			}

			imgProcOperationsBank.addOperation((Operation)op);
		}
		
		//imgProcOperationsBank.display();

		/*** Init best path ***/
		
		bestPath.setBestReward(-1);
		bestPath.setBestBranch(bestBranch);

		/*** Init root ***/
		
		imgProcRoot.level = 0;
		imgProcRoot.setOperationsBank(imgProcOperationsBank);
		imgProcRoot.depthMax = param.depth;
		
		/*** Init simulate policy ***/
		
		imgProcSimPolicy.setOperationsBank(imgProcOperationsBank);
		imgProcSimPolicy.setseed(param.seed);
		
		/*** Init select policy ***/

		imgProcSelectPolicy.setOperationsBank(operationsBank);
		imgProcSelectPolicy.setHyperParameter(param.hyperParam);

		/*** Set learning and test images ***/
		
		Mat img = new Mat();
		Rect rectCrop = new Rect(param.topX, param.topY, param.width, param.height);
		Size size = new Size();
		String name = "";

		/*** Learning set ***/
		
		File repo1 = new File(param.originalImgAddress);
		String[] imgSet1 = repo1.list();
		File repo2 = new File(param.labeledImgAddress);
		String[] imgSet2 = repo2.list();

		for(int i = 0; i < imgSet1.length; i++){
			
			int index = 0;
			img  = Highgui.imread(param.originalImgAddress+"/"+imgSet1[i]);
			Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
			name = imgSet1[i].substring(0, imgSet1[i].indexOf('.'));
			if(param.imgNames.size() == 0){
				index = 0;
			}else{
				index = 0;
				while((index < param.imgNames.size()) && (Integer.parseInt(param.imgNames.get(index)) < Integer.parseInt(name))){
					index++;
				}
			}
			param.imgNames.add(index, name);
			param.originalImg.add(index, img);
			if(param.crop){
				img = img.submat(rectCrop);
			}else if (param.downsample){
				size.height = (int)(img.height()*param.downsamplingRate);
				size.width = (int) (img.width()*param.downsamplingRate);
				Imgproc.resize(img, img, size, param.downsamplingRate, param.downsamplingRate, Imgproc.INTER_LINEAR);
			}
			System.out.println(img.size());
			imgProcReward.input.add(index, img);
			
			for(int j = 0; j < imgSet2.length; j++){
				if(imgSet2[j].contains(name)){
					img  = Highgui.imread(param.labeledImgAddress+"/"+imgSet2[j]);
					Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
					param.labeledImg.add(index, img);
					if(param.crop){
						img = img.submat(rectCrop);
					}else if (param.downsample){
						size.height = (int)(img.height()*param.downsamplingRate);
						size.width = (int) (img.width()*param.downsamplingRate);
						Imgproc.resize(img, img, size, param.downsamplingRate, param.downsamplingRate, Imgproc.INTER_LINEAR);
					}
					imgProcReward.labeled.add(index, img);
					break;
				}
			}
		}
		
		/*** Test set ***/
		
		File testRepo1 = new File(param.testOriginalImgAddress);
		String[] testImgSet = testRepo1.list();
		File testRepo2 = new File(param.testLabeledImgAddress);
		String[] testImgSet2 = testRepo2.list();
		
		for(int i = 0; i < testImgSet.length; i++){

			int index = 0;
			img  = Highgui.imread(param.testOriginalImgAddress+"/"+testImgSet[i]);
			Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
			name = testImgSet[i].substring(0, testImgSet[i].indexOf('.'));
			if(param.imgNames.size() == 0){
				index = 0;
			}else{
				index = 0;
				while((index < param.testImgNames.size()) && (Integer.parseInt(param.testImgNames.get(index)) < Integer.parseInt(name))){
					index++;
				}
			}

			param.testOriginalImg.add(index, img);
			param.testImgNames.add(index, name);
			
			for(int j = 0; j < testImgSet2.length; j++){
				if(testImgSet2[j].contains(name)){
					img  = Highgui.imread(param.testLabeledImgAddress+"/"+testImgSet2[j]);
					Imgproc.cvtColor(img, img, Imgproc.COLOR_BGR2GRAY);
					param.testLabeledImg.add(index, img);
					break;
				}
			}
		}
		
		simPolicy = imgProcSimPolicy;
		selectPolicy = imgProcSelectPolicy;
		operationsBank = imgProcOperationsBank;
		root = imgProcRoot;
		reward = imgProcReward;
		
		/*** Init mcts algorithm ***/

		mcts.setBestPath(bestPath);
		mcts.setBudget(param.budget);
		mcts.setOperationsBank(operationsBank);
		mcts.setReward(reward);
		mcts.setRoot(root);
		mcts.setDepth(param.depth);

		/*** Apply mcts algorithm***/
		
		param.getStartTime();
		try{
			mcts.apply(selectPolicy, simPolicy, param);
			param.setComputationCompleted();
		}catch(Exception e){
			param.getEndTime();
			param.setRunTime();
			System.out.println((char)27 + "[31mComputation not completed!");
			e.printStackTrace();
			param.editLateXReport();
		}		

		if(param.computationCompleted){
			System.out.println((char)27 + "[32mComputation completed");
		}else{
			if(mcts.numCalls == mcts.budget){
				System.out.println((char)27 + "[31mOut of budget!");
			}	
		}
		param.getEndTime();
		param.setRunTime();
		param.editLateXReport();
	}
}