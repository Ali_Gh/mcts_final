import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

import mcib3d.utils.ThreadUtil;

import org.opencv.core.*;

/* Calculates the jaccard score for all the laerning set images
 * and returns the worst one.
 * Multithreading is used to improve the calculation time.
 */
public class ImgProcReward extends Reward{

	public LinkedList<Mat> input;
	public LinkedList<Mat> labeled;
	public LinkedList<Double> jaccardList;
	LinkedList<Integer> seq;
	private static final long MEGABYTE = 1024L * 1024L;

	public ImgProcReward(){

		this.output = new LinkedList<Operation>();
		this.labeled  = new LinkedList<Mat>();
		this.input  = new LinkedList<Mat>();
		this.jaccardList = new LinkedList<Double>();
		this.seq = new LinkedList<Integer>();

	}
	
	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	public LinkedList<LinkedList<Double>> calculate(LinkedList<Operation> filterSeq, Node root){
		
		LinkedList<LinkedList<Double>> output = new LinkedList<LinkedList<Double>>();
		LinkedList<Double> worstOutput = new LinkedList<Double>();
		double worstJaccard = 1.0;
		this.jaccardList.clear();
		for (int k = 0; k < this.input.size(); k++){
			this.jaccardList.add(0.0);
		}
		this.seq.clear();

		//LinkedList<Integer> seq = new LinkedList<Integer>();
		for(int i = 0; i < filterSeq.size(); i++){
			seq.add(filterSeq.get(i).num);
		}

		final AtomicInteger ai = new AtomicInteger(0);

		Thread[] threads = ThreadUtil.createThreadArray(ThreadUtil.getNbCpus());
		for (int ithread = 0; ithread < threads.length; ithread++) {
			threads[ithread] = new Thread() {
				@Override
				public void run() {
					Mat labeledImg = new Mat();
					Mat inputImg = new Mat();
					Mat outputImg = new Mat();
					double jaccard = 0.0;

					for (int k = ai.getAndIncrement(); k < input.size(); k = ai.getAndIncrement()) {

						input.get(k).copyTo(inputImg);
						labeled.get(k).copyTo(labeledImg);

						int success= FiltersBank.runFilters(seq, inputImg, outputImg);//run the list of filters
						if (success==0)
							System.out.println("Running the array of filters failed at one of the filters.");
						//else
						//System.out.println("Running the array of filters successful.");

						jaccard = jaccardScore(labeledImg, outputImg);
						jaccardList.set(k, jaccard);
					}
				}
			};
		}

		ThreadUtil.startAndJoin(threads);

		for(int i = 0; i < jaccardList.size(); i++){
			if(jaccardList.get(i) < worstJaccard){
				worstJaccard = jaccardList.get(i);
			}
		}
		
		worstOutput.add(worstJaccard);
		output.add(worstOutput);
		output.add(jaccardList);
		
		// Get the Java runtime
		Runtime runtime = Runtime.getRuntime();
		// Run the garbage collector
		runtime.gc();

		return output;

	}

	//input: 2 binary images with the same size
	//output: the jaccard score of the two images (|intersection of black pixels|/|union of black pixels|)
	public static double jaccardScore(Mat img1, Mat img2){
		double jaccard= 0;

		//the 2 input images should have the same size
		if (img1.width()==img2.width() && img1.height()==img2.height()){
			int intersection= 0;
			int union= 0;
			for (int i= 0; i< img1.height(); i++) {
				for (int j= 0; j< img1.width(); j++) {
					double[] data1 = img1.get(i, j);
					double[] data2 = img2.get(i, j);
					if (data1[0]<50 && data2[0]<50){ //if the 2 pixels at the same position in the two images are both black
						intersection++;
						union++;
					}
					else if (data1[0]<50 && data2[0]>=50)
						union++;
					else if (data2[0]<50 && data1[0]>=50)
						union++;
				}
			}    
			jaccard= (double)intersection/(double)union;
		}
		else
			return -1;//failed to calculate Jaccard score
		return jaccard;
	}
}
