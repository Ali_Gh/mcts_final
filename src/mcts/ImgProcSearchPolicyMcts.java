/* Implements a uct algorithm, Monte Carlo with a random simulator
 * and UCB-1 select policy
 * uct = mcts(ucb-1, randomsim, N1, N2)
 * 	   = Step(Repeat(N2, Select(Lookahead(Repeat(N1, Simulate)))))	
 */
public class ImgProcSearchPolicyMcts extends SearchPolicy{
	
	public OperationsBank operationsBank;
	public Reward reward;
	public Path bestPath;
	public int depth;
	
	public ImgProcSearchPolicyMcts(){
		super();
		bestPath = new Path();
		depth = 0;
	}
	
	public void setOperationsBank(OperationsBank operationsBank){
		this.operationsBank = operationsBank;
	}
	
	public void setReward(Reward reward){
		this.reward = reward;
	}
	
	public void setBestPath(Path bestPath){
		this.bestPath = bestPath;
	}
	
	public void apply(SelectPolicy selectPolicy,SimPolicy simPolicy, Parameter param){
		        
		Simulate simulator = new Simulate();
		Select select = new Select();
		Repeat repeat1 = new Repeat();
		Repeat repeat2 = new Repeat();
		Step step = new Step();
		LookAhead lookAhead = new LookAhead();

		simulator.setSimPolicy(simPolicy);
		simulator.setOperationsBank(operationsBank);
		simulator.setRoot(root);
		simulator.setDepth(depth);
		simulator.setReward(reward);
		simulator.setRoot(root);
		simulator.setBestPath(bestPath);
		simulator.setSearchPolicy(this);
		simulator.setParam(param);
		
		repeat1.copyParameters(simulator);
		repeat1.setNbRepetitions(param.nbRepetitions1);
		repeat1.setSearchComponent(simulator);
		
		lookAhead.copyParameters(simulator);
		lookAhead.setSearchComponent(repeat1);
		
		select.copyParameters(simulator);
		select.setSelectPolicy(selectPolicy);
		select.setSearchComponent(lookAhead);
		
		repeat2.copyParameters(simulator);
		repeat2.setNbRepetitions(param.nbRepetitions2);
		repeat2.setSearchComponent(select);
		
		step.copyParameters(simulator);
		step.setSearchComponent(repeat2);
		
		step.apply(this.sequence, this.root);		
	}

	public void setDepth(int depth) {
		this.depth = depth;		
	}
	
}
