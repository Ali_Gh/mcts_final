// To link a parent node to its child
/* Includes:
   parent node
   child node
   operatin which links the parent to the child
   */
public class Edge {
	
	 public Node parent;
	 public Node child;
	 public Operation op;
	 
	 public Edge(){
		 
		 setParent(null);
		 setChild(null);
		 setOperation(null);
	 }
	 
	 public void setParent(Node parent){
		 this.parent = parent;
	 }
	 
	 public void setChild(Node child){
		 this.child = child;
	 }
	 
	 public void setOperation(Operation op){
		 this.op = op;
	 }
	 
	 public Node getParent(){
		 return this.parent;
	 }
	 
	 public Node getChild(){
		 return this.child;
	 }
	 
	 public Operation getFilter(){
		 return this.op;
	 }
}
