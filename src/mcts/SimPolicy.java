/* Defines the simulation policy to use
 * 
 */
abstract class SimPolicy {

	abstract Operation apply(Node nextState);

}
