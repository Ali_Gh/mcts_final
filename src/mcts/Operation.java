/* Each operation has at least a name and a number
 * 
 */
abstract class Operation {
	
	public int num;
	public String name;
	
	abstract Node apply(Node child);
}
