import java.util.LinkedList;

/* For each remaining time step, it runs the sub-search component,
 * extracts the next operation from the best sequence obtained so far
 * and performs the transition to the next state. 
 */
public class Step extends SearchComponent{

	public SearchComponent searchComp;

	public Step(){

		super();
		setSearchComponent(null);
		this.name = "Step";
	}

	public void setSearchComponent(SearchComponent serachComp){
		this.searchComp = serachComp;
	}

	public SearchComponent getSearchComponent(){
		return this.searchComp;
	}

	@SuppressWarnings("unchecked")
	public void apply(LinkedList<Operation> sequence, Node state){

		//System.out.println("Step!");

		int t = state.level;
		Node nextState;
		Node copy;
		Operation op;
		LinkedList<Operation> nextSeq = new LinkedList<Operation>();
		nextState = state.copy();
		nextSeq = (LinkedList<Operation>) sequence.clone();
		
		if(t != sequence.size()){
			System.out.println("step problem");
		}

		for(int i = t; i < depth; i++){

			System.out.println("Step to level " + i);
			this.param.setStep(i);

			super.invoke(this.searchComp, nextSeq, nextState); // sub search 

			nextSeq.clear();

			for(int j = 0; j < i+1; j++){
				nextSeq.add(bestPath.bestBranch.get(j));
			}

			op = bestPath.bestBranch.get(i); // extracts next operation 

			if(op.name.equals("Stop")){
				break;
			}
			
			// transition to next state 
			copy = nextState.OperationFindChild(op);
			if(nextState.hasChildren()){
				nextState.children.clear();
			}
			nextState.childrenEdges.clear();
			nextState = copy;
		}
	}
}	