import java.util.LinkedList;

/* For each child, Lookahead computes the successor state
 * and runs the sub-search component.
 */
public class LookAhead extends SearchComponent{

	public SearchComponent searchComp;

	public LookAhead(){
		
		super();
		setSearchComponent(null);
		this.name = "Lookahead";
	}

	public void setSearchComponent(SearchComponent searchComp){
		this.searchComp = searchComp;
	}

	public SearchComponent getSearchComponent(){
		return this.searchComp;
	}

	@SuppressWarnings("unchecked")
	public void apply(LinkedList<Operation> sequence, Node node){
		//System.out.println("Look ahead!");
		Node nextState;
		Operation op;
		LinkedList<Operation> seq = new LinkedList<Operation>();
		
		seq = (LinkedList<Operation>) sequence.clone();
		
		if((seq.size() != 0) && (seq.getLast().name.equals("Stop"))){
			this.param.displayProgress();
			invoke(this.searchComp, seq, node);
			return;
		}
		
		if(node.hasChildren() == false){
			node.expend();			
		}
		if(node.level != sequence.size()){
			System.out.println("lookahead problem");
		}
		
		for(int i = 0; i < node.children.size(); i++ ){

			this.param.setLookahead(i);
			this.param.displayProgress();
			
			nextState = node.copy();
			op = nextState.children.get(i).op;
			seq.add(op);
			nextState = nextState.OperationFindChild(op);
			invoke(this.searchComp, seq, nextState);
			seq.removeLast();
		}
	}
}
