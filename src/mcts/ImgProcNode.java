import java.util.LinkedList;

import org.opencv.core.Mat;

/*Node class for image processing problem
 * extends Node abstract class
 * All the initial images are in grey level
 */

@SuppressWarnings("unused")
public class ImgProcNode extends Node{

	public String state;

	public ImgProcNode(){

		setParent(null);
		this.children = new LinkedList<Node>();
		this.childrenEdges = new LinkedList<Edge>();
		this.nbActionSelect = new int[0];
		this.rewardActionSelect = new double[0];
		this.nbStateSelect = 0;
		setLevel(0);
		this.op = new ImgProcOperation();
		this.state = "Grey";
		this.operationsBank = new ImgProcOperationsBank();
		this.depthMax = 0;
	}

	@SuppressWarnings("unchecked")
	public Node copy(){

		ImgProcNode newNode = new ImgProcNode();

		if(this.children != null){
			for(int j = 0; j < this.children.size(); j++){
				newNode.children.add(this.children.get(j));
			}			
		}
		newNode.setParent(this.parent);
		newNode.childrenEdges.clear();
		newNode.childrenEdges = (LinkedList<Edge>) this.childrenEdges.clone();
		newNode.nbActionSelect = this.nbActionSelect.clone();
		newNode.rewardActionSelect = this.rewardActionSelect.clone();
		newNode.nbStateSelect = this.nbStateSelect;
		newNode.setLevel(this.level);
		newNode.op = this.op;
		newNode.state = this.state;
		newNode.setOperationsBank(this.operationsBank);
		newNode.depthMax = this.depthMax;

		return (Node)newNode;
	}

/*add a child node
 * (non-Javadoc)
 * @see Node#addChild(Node, Operation)
 * Check the filter to apply. If it's a thresholding,
 * the state changes to binary
 */
	public void addChild(Node node, Operation operation){
		ImgProcNode child = new ImgProcNode();
		child = (ImgProcNode) node;
		child.parent = this;
		child.level = this.level + 1;
		child.op = (ImgProcOperation) operation;
		if(operation.name.equals("Thresholding")){
			child.state = "Bin";
		}else{
			child.state = this.state;
		}
		child.setOperationsBank(this.operationsBank);
		child.depthMax = this.depthMax;
		this.children.add(child);
		addChildEdge((Node)child, operation);
	}

/* Expends the current node according to the possible filters:
 * If the node's state is grey level, possible filters are
 * Gabor, Gaussian and thresholding. If the max depth is reached
 * and the node's state still grey level, the only filters possible
 * are thresholding since we need to end with a binary state.
 * If the node's state is binary, only binary filters (dilation, erosion,
 * openning and closing)
 * In all the cases, a child corresponding to the stop filter is created.
 * (non-Javadoc)
 * @see Node#expend()
 */
	public void expend(){

		if(this.children == null){
			this.children = new LinkedList<Node>();
		}else{
			this.children.clear();
		}

		ImgProcOperation op = new ImgProcOperation();

		if(this.state.equals("Grey")){
			if(this.level == this.depthMax-1){
				int grey = ((ImgProcOperationsBank)this.operationsBank).nbGreyScale;
				ImgProcOperation operation = new ImgProcOperation();
				operation = (ImgProcOperation) this.operationsBank.getOperation(grey+1);
				ImgProcNode child = new ImgProcNode();
				child.parent = new ImgProcNode();

				if(operation.num != this.op.num){
					this.addChild((Node)child, (Operation)operation);
				}
			}else{
				int grey = ((ImgProcOperationsBank)this.operationsBank).nbGreyScale + ((ImgProcOperationsBank)this.operationsBank).nbThreshold;
				for (int i = 0; i < grey; i++) {
					ImgProcNode child = new ImgProcNode();
					child.parent = new ImgProcNode();

					op = (ImgProcOperation)this.operationsBank.operations.get(i);
					if(op.num != this.op.num){
						this.addChild((Node)child, (Operation)op);
					}
				}

				ImgProcNode child = new ImgProcNode();
				child.parent = new ImgProcNode();
				op = (ImgProcOperation)this.operationsBank.operations.getLast();
				this.addChild((Node)child, (Operation)op);
			}

		}else if(this.state.equals("Bin")){
			int grey = ((ImgProcOperationsBank)this.operationsBank).nbGreyScale + ((ImgProcOperationsBank)this.operationsBank).nbThreshold;
			int binary = grey + ((ImgProcOperationsBank)this.operationsBank).nbBinary;
			for (int i = grey+1; i < binary+1; i++) {
				ImgProcNode child = new ImgProcNode();
				child.parent = new ImgProcNode();

				op = (ImgProcOperation)this.operationsBank.operations.get(i);
				if(op.num != this.op.num){
					this.addChild((Node)child, (Operation)op);
				}
			}
		}else{
			System.out.println("Expend not possible");
		}

	}

	public boolean hasChildren() {
		return super.hasChildren();
	}

	public void display(){
		System.out.println("Level = " + this.level);
		System.out.println("Operation = number "+Integer.toString(this.op.num)+" "+this.op.name);
		System.out.println("Nb visits = " + this.nbStateSelect);
		for(int i = 0; i < this.nbActionSelect.length; i++){
			System.out.println("Op "+i+" sel "+this.nbActionSelect[i]+" tms "+" rew "+this.rewardActionSelect[i]);
		}
	}

	public void displayFamily(){
		this.display();
		if(this.parent!=null){
			System.out.println("Move to parent!");
			((ImgProcNode) this.parent).displayFamily();
		}
	}
}